
class Developer():
    def __init__(self, age, gender, location, creator, job, botname):
        self.age = age
        self.location = location
        self.creator = creator
        self.gender = gender
        self.job = job
        self.botname =botname

    # returns all properites of developer class
    def getage(self):
        return self.age
    def getlocation(self):
        return self.location
    def getcreator(self):
        return self.creator
    def getgender(self):
        return self.gender
    def getjob(self):
        return self.job
    def getbotname(self):
        return self.botname
