from keywords.keywords import *
from classes.developer_class import Developer
from spellchecker import SpellChecker

def spellcheck(words):
    list = []
    spell = SpellChecker()
    # loops through each word to spell check and give best possible answer as to what the word is 
    for word in words:
    
        list.append(spell.correction(word))
    return list


# finds all words related to keywords and return them in list format
def keywords(words):
    list = []
    for word in words:
        if word in age_keywords:
            list.append(word)
        if word in creator_keywords:
            list.append(word)
        if word in location_keywords:
            list.append(word)
        if word in gender_keywords:
            list.append(word)
        if word in goodbye_keywords:
            list.append(word)
        if word in job_keywords:
            list.append(word)
        if word in greetings_keywords:
            list.append(word)
    return list
            
# returns a response on list of words given only one response is returned
def keyword_response(words,x):

    for word in words:  
            
        if word in age_keywords:
            return print('\nEve🌷> His age is {}'.format(x.getage()))
        

        elif word in gender_keywords:
    
            return print('\nEve🌷> His gender is that of a {}'.format(x.getgender()))
            
        

        elif word in location_keywords:
            return print('\nEve🌷> He resides at {}'.format(x.getlocation()))
        

        elif word in creator_keywords:
            return print('\nEve🌷> His name is {}'.format(x.getcreator()))

        elif word in job_keywords:
            return print('\nEve🌷> {} thats me and well my job is to {} and well my creators job is obiviously a programmer '.format(x.getbotname(), x.getjob()))

        elif word in goodbye_keywords:
            return print('\nEve🌷> bye bye 😀')
        elif word in greetings_keywords:
            return print('''\nEve🌷> Hi I\'m Eve its\' nice to finally talk to someone.\n
        You ask me anything about my developers:
        * gender
        * name
        * age 
        * location
        * my job   
                        ''')
    
    else:
        return print('\nEve🌷> Sorry I\'m not able to understand what you are saying maybe someday my creator will put more keywords in he\'s so lazy ugghhh. 😡😭 I really want to talk to Humans.')