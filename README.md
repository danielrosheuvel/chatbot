
**Description**

This a simple chat bot that is capable of responding to simple sentences based on asking about developers gender, place of living and name along with the bots job. You can also exchange greetings and goodbyes.

**Purpose**

Was used as an evaluation at NexusU

**Install Requirements**

```pip install -r requirements.txt```

**Run**

```py bot.py``` 
or
```python3 bot.py```
