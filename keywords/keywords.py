# keywords used to compare aagainst words given by user
age_keywords = [
    'age',
    'old',
    'lifetime',
    'existed',
    'aged',
    'years',
    'yrs'

]
creator_keywords = [
    'creator',
    'name',
    'alter-ego',
    'aka',
    'nickname'

]
location_keywords = [
    'location',
    'base'
    'home',
    'postion'
    'lodging',
    'live',
    'residence',
    'reside',
    'resides'
    'lives'
    'quarters'

]
gender_keywords = [
    'sex',
    'gender',
    'male',
    'female',
    'orientation'
]
goodbye_keywords = [
    'bye',
    'goodbye',
    'later',
    'farewell'
]
job_keywords = [
    'job',
    'work',
    'duty',
    'function',
    'responsibility',
    'task'
]
greetings_keywords = [
    'hi',
    'hello',
    'gooday'
]